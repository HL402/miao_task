##### 1.js call()和apply()的区别

都是用来改变this指向的。

call传递参数时，传递的数据是以，进行分隔，可以传递多个参数

apply()传递参数时，传递的数据是以数组形式进行传递的。

##### 2.什么是事件冒泡？什么是事件捕获

事件冒泡是由内向外进行触发

事件捕获由外向内进行捕获

[]: https://www.imooc.com/article/303272	"浅析事件捕获和事件冒泡"

##### 3.简述JavaScript中this的指向

1. 全局状态下的this指向

   this指向全局对象window

2. 函数内的this

   分为严格模式和非严格模式

   - 严格模式

     直接调用函数，this指向undefined。

     window.事件名()中this指向window

   - 非严格模式

     直接调用函数和window.事件名()中的this都指向window

3. 对象中的this

   谁调用this就指向谁

   在多级嵌套中的对象中内部this指向被调用函数最近的对象

4. 箭头函数中的this

   箭头函数中没有this，会先从父级作用域中进行查找，如果父级作用域还是箭头函数，就继续往上找，一层一层的进行查找，最后指向window

5. 构造函数中的this

   构造函数中的this指向构造函数下创建的实例

6. 原型链中的this

   this先在调用它的对象中进行查找，如果没找到的话就沿着原型链中进行查找，就继续沿着原型向下继续查找。

##### 4.get和post有什么区别？  

|                |                             get                              |                             post                             |
| :------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|    一般用途    |                           获取数据                           |                           提交数据                           |
|      参数      | 参数会放置在url地址栏中，所以隐私性和安全性会比较差，并且请求数据的参数长度是由限制的 |           post请求的参数放置在body中，没有长度限制           |
|    刷新页面    |              刷新页面不影响，还是离开之前的页面              |               post刷新页面会重新进行请求服务器               |
|    请求缓存    |                         请求会被缓存                         |                        请求不会被缓存                        |
|    请求保存    |             get请求会被保存在浏览器历史记录当中              |                           post不会                           |
|    编码方式    |                    get请求只能进行url编码                    |                    post请求有多种编码方式                    |
|   产生数据包   |                    get通常产生一个数据包                     |                  post请求通常产生两个数据包                  |
| 浏览器发送方式 | get请求浏览器会将header和data一并进行发送服务器，服务器响应200，代表响应成功 | post请求浏览器会先将header发送服务器进行请求，服务器响应100，浏览器在进行发送data，服务器参会响应200成功 |

##### 5.如何防抖？

在全局中进行声明一个变量为空 let timer=null

在输入事件中首先判断timer是否为空 如果不为空的话我们就需要进行请求定时器（后面会定义定时器）

就下了设置一个定时器 this.timer=setTimeOut(()=>{请求接口的事件},1000)

##### 6.http请求头中常见content-type种类有哪些？

content-type：

用来定义网络文件的类型和网页的编码，用来决定浏览器将以什么形式、什么编码读取这个文件

|          | **application/x-www-form-urlencoded** |  **multipart/form-data**   |                  **application/json**                  |              binary (application/octet-stream)               | text/xml                                                     |
| :------: | :-----------------------------------: | :------------------------: | :----------------------------------------------------: | :----------------------------------------------------------: | ------------------------------------------------------------ |
|  请求体  |                键值对                 |      boundary格式字串      |                        JSON字串                        |                         二进制(get)                          |                                                              |
| 上传文件 |                不支持                 |            支持            |                         不支持                         |                             支持                             |                                                              |
| 使用场合 |             长传简单文本              |       文件、文本混传       | 复杂结构文本，JSON格式支持比键值对复杂得多的结构化数据 | 只传文件，只能提交二进制文件，而且只能提交一个二进制。如果提交文件的话，只能提交一个文件，后台接收参数只有一个，而且只能是流 | 提供统一的方法来描述和交换独立于应用程序或供应商的结构化数据 |
|          |                默认值                 | 以表单形式进行提交，文件流 |                   以json对象进行传输                   |                        二进制文件类型                        | 传输和存储数据                                               |

[参考文档]: https://www.cnblogs.com/wushifeng/p/6707248.html	"四种常见的 POST 提交数据方式对应的content-type取值"
[参考文档]: http://t.zoukankan.com/xidianzxm-p-14241657.html	"HTTP请求中几种常见的Content-Type类型"

##### 7.描述vue模板编译原理

- 流程

  1. 解析器 将模板字符串转化为语法树	

  2. 优化器

     找出静态节点和静态根节点

     采用递归的方式将所有的节点打标记，表示是否是一个静态节点，然后再次递归一遍把静态根节点找出来

  3. 代码生成器

     通过递归去拼一个函数执行代码的字符串，递归的过程根据不同的节点类型来进行调用不同的生成方法

- 原理

  vue会根据其规定的的模板语法规则，将其解析成AST语法树(其实就是用树状的大对象来描述我们所谓的HTML)，然后会对这个大对象进行一些初步处理，比如标记没有动态绑定的节点，最后会把这个大对象编译成render函数，并将他绑定在组件的实例上。这样，我们所认为的HTML就变成了JavaScript，可以基于JavaScript模块规则进行导入导出，在需要渲染组件的地方，就调用render函数，根据组件当前的状态生成虚拟dom，然后就可以根据这个虚拟dom去更新视图了。

[参考文档]: https://juejin.cn/post/6999525002592649252

##### 8.hash路由和history路由原理简单说下

- hash模式

  当hash发送改变就会触发onhashchange事件	从而进行页面的跳转

- history模式 

  通过window提供的popState事件来监听历史栈的改变，只要历史栈有信息发生改变的话就会触发该事件

  通过H5新增的特性pushState和replaceState来进行实现页面跳转 这两个方法改变url页面也不会重新刷新，但是url地址会改变

  1. pushState 向历史记录中追加一条历史记录
  2. replaceState 替换当前页在历史记录中的信息

##### 9.vue中watch/computed的区别

computed是基于响应性依赖来进行缓存的。可以实现对数据的加工

watch不具有依赖性，实现对数据的监听

##### 10.vue中组件通信方式有哪些

- 父传子

  父组件向子组件传递数据v-bind，子组件通过props来进行接收父组件传递的数据

- 子传父

  子组件事件绑定通过$emit来进行传递数据，$emit('向父组件绑定的事件',传递的数据)

  然后父组件通过在触发的地方来进行监听事件@子组件定义的事件名="父组件定义的事件名"来进行双向绑定数据，在methods来进行绑定方法，接收参数e，通过e.detail来进行接收子组件向父组件传递的数据

- 兄弟组件通信

  eventBus又叫中央时间总线，先声明一个实例bus，发布订阅者模式

  发送数据是通过$bus.$emit

  接收数据是通过$bus.$on

- vuex

  vue的状态管理工具，将组件中的状态进行存储

[参考网站]: https://www.jianshu.com/p/23b97e324b50

##### 11.关于webpack是否写过Loader和Plugin？描述一下编写loader或Plugin的思路

webpack中包含了各种plugin（插件），然后plugin中又有entry(文件加入) loader(对文件进行处理) output(输出)步骤操作

- Loader 是文件加载器，能够加载资源文件，并对这些文件进行一些处理 

  运行在打包文件之前

- Plugin（插件） 被webpack赋予了各种灵活的功能 目的是解决loader无法实现的一些事 

  在整个编译周期都起作用

  plugin也是为了扩展webpack的功能，但是 plugin 是作用于webpack本身上的。而且plugin不仅只局限在打包，资源的加载上，它的功能要更加丰富。从打包优化和压缩，到重新定义环境变量，功能强大到可以用来处理各种各样的任务。webpack提供了很多开箱即用的插件：CommonChunkPlugin主要用于提取第三方库和公共模块，避免首屏加载的bundle文件，或者按需加载的bundle文件体积过大，导致加载时间过长，是一把优化的利器。而在多页面应用中，更是能够为每个页面间的应用程序共享代码创建bundle 

1. 编写loader

   loader本质为函数，函数中的this作为上下文会被webpack填充，因此我们不能加loader设置为一个箭头函数

   函数接收一个参数，为webpack传递给loader的文件源内容

   函数中的this是webpack提供的对象，能够获取到当前loader所需要的各种信息

   函数中有异步操作和同步操作 

   异步操作提供`this.callback`返回 返回值要求为string或者是buffer

   一般在编写loader的过程中 保持功能单一，避免做多种功能

   如`less`文件转换成 `css`文件也不是一步到位，而是 `less-loader`、`css-loader`、`style-loader`几个 `loader`的链式调用才能完成转换

2. 编写plugin

   [参考文档]: https://wenku.baidu.com/view/022e8695950590c69ec3d5bbfd0a79563c1ed47a.html
   
   由于webpack基于发布订阅模式，在运行的周期中会广播出许多事情，插件通过监听这些事件，就可以在特定的阶段执行自己的插件

[参考文档1.1]: https://blog.csdn.net/thewar196/article/details/122801614	"webpack中Loader和Plugin的区别、编写Loader，Plugin的思路"
[参考文档1.0]: https://blog.csdn.net/thewar196/article/details/122801614

##### 12.有哪些常见的webpack Loader？他们是怎么解决问题的？

`1、file-loader：`把⽂件输出到⼀个⽂件夹中，在代码中通过相对 URL 去引⽤输出的⽂件。
`2、url-loader：`和 file-loader 类似，但是能在⽂件很⼩的情况下以 base64 的⽅式把⽂件内容注⼊到代码中去。
`3、source-map-loader：`加载额外的 Source Map ⽂件，以⽅便断点调试。
`4、image-loader：`加载并且压缩图⽚⽂件。
`5、babel-loader：`将ES6转化为ES5。
`6、css-loader：`加载 CSS，⽀持模块化、压缩、⽂件导⼊等特性。
`7、style-loader：`把 CSS 代码注⼊到 JavaScript 中，通过 DOM 操作去加载 CSS。
`8、eslint-loader:`通过 ESLint 检查 JavaScript 代码。

