#### 1.Mixin

Mixin是面向对象程序设计语言中的类，提供了方法的实现。其他类可以访问mixin类的方法而不必成为其子类

mixin和组件的区别：

- 组件

  父组件+子组件 》》》父组件+子组件

  组件在引入之后相当于在父组件内开辟了一块单独的空间，来根据父组件props过来的值进行相应的操作，但是本质上父组件和子组件还是泾渭分明，相对独立。

- mixin

  父组件+子组件 》》》new父组件

  mixin在引入之后是将组件内部的内容如data等方法、methods等属性与父组件相应内容进行合并。相当于在引入之后父组件的各种属性方法都被扩充了。 

在借助mixin之前，在两个不同的组件的组件中调用foo方法，需要重复定义，倘若方法比较复杂，代码将更加冗余。若借助mixin，则会变得非常简单

```js
let mixin={
	data(){
        return{
            msg:1
        }
    },
    methods:{
        foo(){
            console.log("hello from mixin---"+this.msg++)
        }
    }
}
var child={
    Vue.component("child",{
    	template:`<h1 @click="foo">child component</h1>`,
    	mixins:[mixin]
	})
}
Vue.component("kid",{
    template:`<h1 @click="foo">kid component</h1>`,
    mixins:[mixin]
})
```

虽然此处，两个组件用可以通过this.msg来进行引用mixins中定义的msg，但是两个组件中引用的并不是同一个msg，而是各自创建了一个新的msg。如果在组件中定义相同的data，则此处会引用组件中的msg，而非mixins中的

如果在引用mixins的同时，在组件中重复定义相同的方法，则mixins中的方法会被覆盖

[参考文档]: https://www.jianshu.com/p/a72bf060eeaa

#### 2.有哪些常见的webpack Loader？他们是怎么解决问题的？

`1、file-loader：`把⽂件输出到⼀个⽂件夹中，在代码中通过相对 URL 去引⽤输出的⽂件。
`2、url-loader：`和 file-loader 类似，但是能在⽂件很⼩的情况下以 base64 的⽅式把⽂件内容注⼊到代码中去。
`3、source-map-loader：`加载额外的 Source Map ⽂件，以⽅便断点调试。
`4、image-loader：`加载并且压缩图⽚⽂件。
`5、babel-loader：`将ES6转化为ES5。
`6、css-loader：`加载 CSS，⽀持模块化、压缩、⽂件导⼊等特性。
`7、style-loader：`把 CSS 代码注⼊到 JavaScript 中，通过 DOM 操作去加载 CSS。
`8、eslint-loader:`通过 ESLint 检查 JavaScript 代码。

####  3.事件冒泡、事件捕获、事件委托

事件冒泡：目标元素事件先触发，然后父元素事件触发

事件捕获：父元素事件先出发，然后目标元素事件触发

事件的执行顺序：

​	先事件捕获(从windows--->document 依次往下)

​	再是目标事件处理

​	最后是事件冒泡

addEventListener 

[参考文档]: https://cloud.tencent.com/developer/article/1862015

#### 4.webpack

[参考文档]: https://baijiahao.baidu.com/s?id=1708753582745612917&amp;wfr=spider&amp;for=pc

`webpack `是一个用于现代`JavaScript`应用程序的静态模块打包工具

- ##### 静态模块

  这里的静态模块指的是开发阶段，可以被`webpack`直接引用的资源（可以直接被获取打包进`bundle.js`的资源） 

  当 webpack处理应用程序时，它会在内部构建一个依赖图，此依赖图对应映射到项目所需的每个模块（不再局限js文件），并生成一个或多个 `bundle`

`webpack`的能力：

「编译代码能力」，提高效率，解决浏览器兼容问题

「模块整合能力」，提高性能，可维护性，解决浏览器频繁请求文件的问题

「万物皆可模块能力」，项目维护性增强，支持不同种类的前端模块类型，统一的模块化方案，所有资源文件的加载都可以通过代码控制

- `webpack`执行流程

  `webpack`可以理解为是一种基于事件流的编程规范，`plugin`会订阅相应的hook功能。从`entry`开始进行传入文件，递归遍历查找所有的依赖并匹配相应的`loader`进行转化为最终的文件，`output`输出文件。

  同时在整个编译过程中，调用不同的钩子，运行一系列的插件。
  
- **流程**:

  1. 首先通过配置的entry入口，找到入口文件main.js
  2. 通过配置的各种loader(scss-loader配合正则找所有的css样式文件，file-loader配合正则找图片文件转成base64;vue-loader找所有的vue文件)统一打包成js和css的chunk文件
  3. 通过插件(html-webpack-plugin插件),把这种chunk文件(打包生成的文件)自动嵌入到index.html

#### 4.关于webpack是否写过Loader和Plugin？描述一下编写loader或Plugin的思路

webpack中包含了各种plugin，然后plugin中又有entry(文件加入) loader(对文件进行处理) output(输出)步骤操作

- Loader 是文件加载器，能够加载资源文件，并对这些文件进行一些处理
- Plugin 被webpack赋予了各种灵活的功能 目的是解决loader无法实现的一些事  

[参考文档]: https://blog.csdn.net/thewar196/article/details/122801614

#### 5.描述vue模板编译原理

- 流程

  1. 解析器 将模板字符串转化为语法树	

  2. 优化器

     找出静态节点和静态根节点

     采用递归的方式将所有的节点打标记，表示是否是一个静态节点，然后再次递归一遍把静态根节点找出来

  3. 代码生成器

     通过递归去拼一个函数执行代码的字符串，递归的过程根据不同的节点类型来进行调用不同的生成方法

- 原理

  vue会根据其规定的的模板语法规则，将其解析成AST语法树(其实就是用树状的大对象来描述我们所谓的HTML)，然后会对这个大对象进行一些初步处理，比如标记没有动态绑定的节点，最后会把这个大对象编译成render函数，并将他绑定在组件的实例上。这样，我们所认为的HTML就变成了JavaScript，可以基于JavaScript模块规则进行导入导出，在需要渲染组件的地方，就调用render函数，根据组件当前的状态生成虚拟dom，然后就可以根据这个虚拟dom去更新视图了。

#### 6.vue-cli替我们做了哪些工作？

vue-cli是基于vue.js进行快速开发的完整系统，也可以理解成是很多npm的集合。

- 完成的功能
  1. `.vue`文件转化为`.js`
  2. ES6语法转化为ES5语法
  3. `Sass,Less,Style`转化为`CSS`
  4. 对`jpg,png,font`等静态资源的处理
  5. 热更新
  6. 定义环境变量，区分`dev`和`production`模式

vue-cli是vue的一个脚手架工具，主要作用：目录结构 本地调试 代码部署 热加载 单元测试

#### 7.ES6新增特性之新增数据结构 Set Map

- Set

  

- Map

  只能保存键值对。任何值（对象或者是原始值）都可以作为一个键或者值，构造函数Map可以接受一个数组作为参数

  ##### Map对象的属性

  - size：返回Map对象中所包含的键值对个数

  ##### Map对象的方法

  - set(key,val) 向Map中添加新元素
  - get(key) 获取Map对象中特定的键值对
  - has(key) 判断Map对象中是否有`Key`所对应的值,有就返回`true`,否则返回`false`
  - delete(key) 通过键值从Map对象上删除对应的数据
  - clear() 将这个Map对象中所有元素删除

  ##### 遍历方法

  - keys() 返回键名的遍历器
  - values() 返回键值的遍历器
  - entries() 返回键值对的遍历器
  - forEach() 使用回调函数遍历每个成员

  ##### Map和Object的区别

  1. 一个Object的键只能是字符串或者是Symbols，但一个Map的键可以是任意值
  2. Map中的键值是有序的（FIFO原则），而添加到对象中的键不是
  3. Map中的键值对个数可以通过size属性获取，而Object的键值对个数只能手动计算
  4. Object都有自己的原型，原型链上的键名有可能和你对象上设置的键名产生冲突


#### 8.简述JavaScript中this的指向

1. 全局状态下的this指向

   this指向全局对象window

2. 函数内的this

   分为严格模式和非严格模式

   - 严格模式

     直接调用函数，this指向undefined。

     window.事件名()中this指向window

   - 非严格模式

     直接调用函数和window.事件名()中的this都指向window

3. 对象中的this

   谁调用this就指向谁

   在多级嵌套中的对象中内部this指向被调用函数最近的对象

4. 箭头函数中的this

   箭头函数中没有this，会先从父级作用域中进行查找，如果父级作用域还是箭头函数，就继续往上找，一层一层的进行查找，最后指向window

5. 构造函数中的this

   构造函数中的this指向构造函数下创建的实例

6. 原型链中的this

   this先在调用它的对象中进行查找，如果没找到的话就沿着原型链中进行查找，就继续沿着原型向下继续查找。

#### 9.什么是Sass，什么是Less

Sass和Less都属于CSS预处理器

通俗的讲，就是一种专门的编程语言，进行web页面样式设计，再通过编译器转化为正常的CSS文件

- 区别

  1. Less环境较Sass简单

     Sass的环境需要安装Ruby环境，Less基于JavaScript，是需要引入Less.js来处理代码输出css到浏览器，也可以在开发环境使用Less，然后编译成css文件，直接放在项目中，有less.app、SimpleLess、CodeKit.app这样的工具，也有在线编辑地址

  2. Less使用较Sass简单

  3. Sass较Less略强大一些

     Sass有变量和作用域；Sass有函数的概念；进程控制；数据结构

  4. Less与Sass处理机制不一样

     前者是通过客户端处理的，后者是通过服务端处理，相比较之下前者解析会比后者慢一点。

  5. 变量在Less和Sass中的唯一区别就是

     Less用 @ , Sass用 $